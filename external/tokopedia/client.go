package external_tokopedia

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gocolly/colly"
)

type Client interface {
	GetTopPhone(ctx context.Context, start int32) (*ProductQueryResponse, error)
	CreateFingerprint(ctx context.Context) context.Context
}
type client struct {
	BaseURL        string
	BaseGraphqlURL string
	Scraper        *colly.Collector
}

func NewClient(baseURL, baseGraphqlURL string, colly *colly.Collector) Client {
	return &client{baseURL, baseGraphqlURL, colly}
}

// GetTopPhone Get Top Phone Currently there are only max 60 rows per query, so we hard code the limit to 50 for the easy calculation sake
// We use offset for each request (start from 1)
// If we would like to get 100 data we need to invoke this function twice with start=1 & start=51
func (c *client) GetTopPhone(ctx context.Context, start int32) (*ProductQueryResponse, error) {
	url := fmt.Sprintf(c.BaseGraphqlURL)

	newScrapper := colly.NewCollector()
	c.setGQLHTTPHeader(ctx, newScrapper)

	res := &ProductQueryResponse{}
	newScrapper.OnScraped(func(r *colly.Response) {
		if err := json.Unmarshal(r.Body, res); err != nil {
			panic(err)
		}
	})
	payload := []byte(fmt.Sprintf("{\"query\":\"query SearchProductQuery($params: String) {\\n  CategoryProducts: searchProduct(params: $params) {\\n    data: products {\\n      id\\n      url\\n      imageUrl: image_url\\n      catId: category_id\\n      name\\n      price\\n      rating\\n      description\\n      shop {\\n        id\\n        url\\n        name\\n      }\\n    }\\n  }\\n  \\n}\",\"variables\":{\"params\":\"page=1&ob=&identifier=handphone-tablet_handphone&sc=24&user_id=0&rows=10&start=%d&source=directory&device=desktop&page=1&related=true&st=product&safe_search=false\"},\"operationName\":\"SearchProductQuery\"}", start))
	if err := newScrapper.PostRaw(url, payload); err != nil {
		return nil, err
	}

	return res, nil

}

func (c *client) setGQLHTTPHeader(ctx context.Context, scrapper *colly.Collector) {
	ctxHTTPCookies := ctx.Value("cookies")
	cookies, ok := ctxHTTPCookies.([]*http.Cookie)
	if !ok {
		panic("There is no HTTP Headers")
	}

	scrapper.OnRequest(func(r *colly.Request) {

		// if c.Scraper.Cookies(r.URL.String()) == nil {
		// 	panic("There is no Cookie")
		// }

		// if r.Headers.Get("Cookie") == "" {
		// 	panic("There is no Cookie")
		// }
		// r.Headers.Set("Cookie", c.Scraper.Cookies(r.URL.String()))

		for _, cookie := range cookies {
			r.Headers.Add("Cookie", cookie.Value)
		}
		r.Headers.Set("Host", `gql.tokopedia.com`)
		r.Headers.Set("tkpd-userid", "0")
		r.Headers.Set("user-agent", `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Edg/94.0.992.38`)
		r.Headers.Set("content-type", "application/json")
		r.Headers.Set("accept", `*/*`)
		r.Headers.Set("x-version", "c2e2ec6")
		r.Headers.Set("x-source", "tokopedia-lite")
		r.Headers.Set("x-device", "desktop-0.0")
		r.Headers.Set("x-tkpd-lite-service", "zeus")
		r.Headers.Set("sec-ch-ua", `"Chromium";v="94", "Microsoft Edge";v="94", ";Not A Brand";v="99"`)
		r.Headers.Set("sec-ch-ua-mobile", `?0`)
		r.Headers.Set("sec-ch-ua-platform", `"Windows"`)
		r.Headers.Set("sec-fetch-site", `same-site`)
		r.Headers.Set("sec-fetch-mode", `cors`)
		r.Headers.Set("sec-fetch-dest", `empty`)
		r.Headers.Set("origin", `https://www.tokopedia.com`)
		r.Headers.Set("referer", `https://www.tokopedia.com/p/handphone-tablet/handphone?page=1`)
		r.Headers.Set("accept-language", `en-US,en;q=0.9`)

	})
}

func (c *client) CreateFingerprint(ctx context.Context) context.Context {
	c.Scraper.OnRequest(func(r *colly.Request) {
		r.Headers.Set("Host", `www.tokopedia.com`)
		r.Headers.Set("sec-ch-ua", `"Chromium";v="94", "Microsoft Edge";v="94", ";Not A Brand";v="99"`)
		r.Headers.Set("sec-ch-ua-mobile", `?0`)
		r.Headers.Set("sec-ch-ua-platform", `"Windows"`)
		r.Headers.Set("sec-fetch-site", `none`)
		r.Headers.Set("sec-fetch-dest", `document`)
		r.Headers.Set("sec-fetch-mode", `navigate`)
		r.Headers.Set("sec-fetch-user", `?1`)
		r.Headers.Set("upgrade-insecure-requests", `1`)
		r.Headers.Set("user-agent", `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Edg/94.0.992.38`)
		r.Headers.Set("accept", `text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9`)
		r.Headers.Set("accept-language", `en-US,en;q=0.9`)

	})

	c.Scraper.OnScraped(func(r *colly.Response) {
		ctx = context.WithValue(ctx, "cookies", c.Scraper.Cookies(r.Request.URL.String()))

	})

	if err := c.Scraper.Visit("https://wwww.tokopedia.com/"); err != nil {
		panic(err)
	}

	return ctx
}
