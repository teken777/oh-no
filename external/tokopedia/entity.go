package external_tokopedia

type ProductQueryResponse struct {
	Data ProductData `json:"data"`
}

type ProductData struct {
	Data CategoryProduct `json:"CategoryProducts"`
}

type CategoryProduct struct {
	Data []Product `json:"data"`
}

type Product struct {
	ID          int64  `json:"id"`
	URL         string `json:"url"`
	ImageURL    string `json:"imageUrl"`
	CatID       int64  `json:"catId"`
	Name        string `json:"name"`
	Price       string `json:"price"`
	Rating      int    `json:"rating"`
	Description string `json:"description"`
	Shop        Shop   `json:"shop"`
}

type Shop struct {
	ID   int    `json:"id"`
	URL  string `json:"url"`
	Name string `json:"name"`
}
