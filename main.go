package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"os"

	external_tokopedia "github.com/farhaan/scraping/tokopedia/external/tokopedia"
	"github.com/gocolly/colly"
)

// Flow

// - Scrape Random URL
// - Get Fingerprinted Data (in cookies)
// - Invoke graphql
// - Store In CSV

func main() {
	scraper := colly.NewCollector()
	tokopediaCli := external_tokopedia.NewClient("https://www.tokopedia.com", "https://gql.tokopedia.com/", scraper)
	ctx := context.Background()
	ctx = tokopediaCli.CreateFingerprint(ctx)

	var topPhone []external_tokopedia.Product

	response, err := tokopediaCli.GetTopPhone(ctx, 1)
	if err != nil {
		panic(err)
	}

	topPhone = append(topPhone, response.Data.Data.Data...)

	response, err = tokopediaCli.GetTopPhone(ctx, 51)
	if err != nil {
		panic(err)
	}

	topPhone = append(topPhone, response.Data.Data.Data...)

	CreateCSV(topPhone)

}

func CreateCSV(topPhones []external_tokopedia.Product) {
	csvFile, err := os.OpenFile("./report.csv", os.O_CREATE|os.O_APPEND|os.O_WRONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}

	writer := csv.NewWriter(csvFile)
	defer func() {
		writer.Flush()
		csvFile.Close()
	}()

	for _, product := range topPhones {
		var row []string
		row = append(row, product.Name)
		row = append(row, product.Description)
		row = append(row, product.ImageURL)
		row = append(row, product.Price)
		row = append(row, fmt.Sprintf("%d", product.Rating))
		row = append(row, product.Shop.Name)
		writer.Write(row)
	}
}
